#!/usr/bin/python3
# -*- coding: utf-8 -*-
# rapid dev script to add token length of sentence to the parse

import sys, re, os
from datetime import datetime


def main():
  #input_file = open("corpus_l.txt", "r", encoding="utf8")
  input_file = open("corpus_cleaned.txt", "r", encoding="utf8")
  output_file = open("corpus_ls.txt", "w", encoding="utf8")

  input_file_as_string = input_file.read()
  parses = input_file_as_string.split("\n\n")
  for parse in parses:
    lines = parse.split("\n")
    lines_without_punct = []
    for line in lines:
      line = line.strip()
      line_elements = line.split("\t")
      if len(line_elements) > 8:
        lemma = line_elements[2]
        if "\tpunct\t" not in line and lemma != "_":
          lines_without_punct.append(line)
    for line in lines:
      line = line.strip()
      if len(line) > 0:
        output_file.write(line + "\t" + str(len(lines_without_punct)) + "\n")
      else:
        output_file.write(line + "\n")
    output_file.write("\n")


  input_file.close()
  output_file.close()


# boilerplate
if __name__ == "__main__":
  startTime = datetime.now()
  main()
  print(datetime.now() - startTime)
else:
  print("Script can only run as a stand-alone.")

