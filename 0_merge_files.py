#!/usr/bin/python3
# -*- coding: utf-8 -*-
# rapid dev script to merge UD corpora of the same language

import sys, re, os
from datetime import datetime


def main():
  #input_file = open("corpus_normalized_text_only.txt", "r", encoding="utf8")
  output_file = open("corpus_merged.txt", "w", encoding="utf8")

  #input_file_as_string = input_file.read()
  for input_file_name in os.listdir("."):
    if input_file_name.endswith(".conllu"):
      input_file = open(input_file_name, "r", encoding="utf8")
      for line in input_file:
        line = line.strip()
        output_file.write(line + "\n")

      input_file.close()
  output_file.close()


# boilerplate
if __name__ == "__main__":
  startTime = datetime.now()
  main()
  print(datetime.now() - startTime)
else:
  print("Script can only run as a stand-alone.")

