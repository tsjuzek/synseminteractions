#!/usr/bin/python3
# -*- coding: utf-8 -*-
# rapid dev script to add dependency length to the tokens of a parse

import sys, re, os
from datetime import datetime
import math


def main():
  input_file = open("corpus_lscf.txt", "r", encoding="utf8")
  output_file = open("corpus_lscfd.txt", "w", encoding="utf8")

  for line in input_file:
    line = line.strip()
    line_elements = line.split("\t")
    if len(line_elements) > 8:
      dep_id = line_elements[0]
      dep_ids = dep_id.split("-")
      dep_id = dep_ids[0]
      dep_id = int(round(float(dep_id[0]), 0))
      try:
        head = int(line_elements[6])
      except:
        head = dep_id
      if head == 0:
        dep_id = 0
      distance = abs(head - dep_id)
      output_file.write(line + "\t" + str(distance) + "\n")
    else:
      output_file.write(line + "\n")

  input_file.close()
  output_file.close()


# boilerplate
if __name__ == "__main__":
  startTime = datetime.now()
  main()
  print(datetime.now() - startTime)
else:
  print("Script can only run as a stand-alone.")

