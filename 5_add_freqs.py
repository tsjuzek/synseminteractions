#!/usr/bin/python3
# -*- coding: utf-8 -*-
# rapid dev script to add absolute freq of a lemma to the token

import sys, re, os
from datetime import datetime
import math


def main():
  input_file = open("corpus_lsc.txt", "r", encoding="utf8")
  output_file = open("corpus_lscf.txt", "w", encoding="utf8")
  output_file_debug = open("x_debug.txt", "w", encoding="utf8")
  input_file_as_string = input_file.read()
  word_freq_dct = dict()
  lemma_freq_dct = dict()

  input_file_lines = input_file_as_string.split("\n")
  for line in input_file_lines:
    line_elements = line.split("\t")
    if len(line_elements) > 8:
      orth = line_elements[1]
      pos = line_elements[3]
      lemma = line_elements[2]
      if pos != "NNP":
        orth = orth.lower()
      if orth not in word_freq_dct:
        word_freq_dct[orth] = 1
      else:
        word_freq_dct[orth] += 1
      if lemma not in lemma_freq_dct:
        lemma_freq_dct[lemma] = 1
      else:
        lemma_freq_dct[lemma] += 1


  parses = input_file_as_string.split("\n\n")
  for parse in parses:
    lines = parse.split("\n")
    for line in lines:
      line = line.strip()
      line_elements = line.split("\t")
      if len(line_elements) > 8:
        orth = line_elements[1]
        pos = line_elements[3]
        lemma = line_elements[2]
        if pos != "NNP":
          orth = orth.lower()
        word_freq = word_freq_dct[orth]
        word_freq_log = round(math.log10(word_freq), 3)
        lemma_freq = lemma_freq_dct[lemma]
        lemma_freq_log = round(math.log10(lemma_freq), 3)
        output_file.write(line + "\t" + str(word_freq_log) + "\t" + str(lemma_freq_log) + "\n")
      else:
        output_file.write(line + "\n")
    output_file.write("\n")

  for w in sorted(lemma_freq_dct, key=lemma_freq_dct.get, reverse=True):
    output_file_debug.write(w + "\t" + str(lemma_freq_dct[w]) + "\n")

  input_file.close()
  output_file.close()
  output_file_debug.close()


# boilerplate
if __name__ == "__main__":
  startTime = datetime.now()
  main()
  print(datetime.now() - startTime)
else:
  print("Script can only run as a stand-alone.")

