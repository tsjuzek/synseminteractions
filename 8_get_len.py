#!/usr/bin/python3
# -*- coding: utf-8 -*-
# rapid dev script to get median token length per sequence including median char length for that median token length

import sys, re, os
from datetime import datetime
import statistics


def main():
  input_file = open("corpus_lscfdnp.txt", "r", encoding="utf8")
  output_file = open("x_corpus_info_lengths.txt", "w", encoding="utf8")

  string_of_sent_lens = "sent_len\n"
  string_of_char_lens = "char_len\n"
  list_of_sent_lens = list()
  list_of_char_lens = list()
  list_of_targeted_char_lens = list()
  list_of_targeted_char_lens_long = list()
  input_file_as_string = input_file.read()
  parses = input_file_as_string.split("\n\n")
  for parse in parses:
    lines = parse.split("\n")
    if len(lines) > 2:
      lines_elements = lines[0].split("\t")
      if len(lines_elements) > 8:
        sent_len = int(lines_elements[10])
        char_len = int(lines_elements[11])
        string_of_sent_lens += str(sent_len) + "\t"
        string_of_char_lens += str(char_len) + "\t"
        list_of_sent_lens.append(sent_len)
        list_of_char_lens.append(char_len)
  try:
    most_freq_sent_len = max(set(list_of_sent_lens), key = list_of_sent_lens.count)
  except:
    most_freq_sent_len = 0
  try:
    most_freq_char_len = max(set(list_of_char_lens), key = list_of_char_lens.count)
  except:
    most_freq_char_len = 0

  try:
    median_sent_len = statistics.median(list_of_sent_lens)
  except:
    median_sent_len = 0
  try:
    median_char_len = statistics.median(list_of_char_lens)
  except:
    median_char_len = 0

  for parse in parses:
    lines = parse.split("\n")
    if len(lines) > 2:
      lines_elements = lines[0].split("\t")
      if len(lines_elements) > 8:
        sent_len = int(lines_elements[10])
        char_len = int(lines_elements[11])
        if sent_len == most_freq_sent_len:
          list_of_targeted_char_lens.append(char_len)
        if sent_len == int(most_freq_sent_len * 2):
          list_of_targeted_char_lens_long.append(char_len)

  try:
    most_freq_char_len_targeted = max(set(list_of_targeted_char_lens), key = list_of_targeted_char_lens.count)
  except:
    most_freq_char_len_targeted = 0
  try:
    most_freq_char_len_targeted_long = max(set(list_of_targeted_char_lens_long), key = list_of_targeted_char_lens_long.count)
  except:
    most_freq_char_len_targeted_long = most_freq_char_len_targeted * 2

  dir_path = os.path.dirname(os.path.realpath(__file__))
  dir_path_elements = dir_path.split("/")
  language = dir_path_elements[-1]

  output_file.write(language + " " + str(most_freq_sent_len) + " " + str(most_freq_char_len_targeted) + " " + str(most_freq_char_len_targeted_long))

  input_file.close()
  output_file.close()


# boilerplate
if __name__ == "__main__":
  startTime = datetime.now()
  main()
  print(datetime.now() - startTime)
else:
  print("Script can only run as a stand-alone.")

