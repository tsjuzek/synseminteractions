#!/usr/bin/python3
# -*- coding: utf-8 -*-
# rapid dev script to analyse the parses for sequences of the median token length that are within a certain char length. outputs correlation between sum of log frequencies and sum of dependency distances

import sys, re, os
from datetime import datetime
import statistics
import scipy.stats


def main():
  input_file = open("corpus_lscfdnp.txt", "r", encoding="utf8")
  input_file_parameters = open("x_corpus_info_lengths.txt", "r", encoding="utf8")
  output_file_debug = open("x_debug.txt", "w", encoding="utf8")
  output_file_correlation = open("/home/~~~/deplen_freq/correlations.txt", "a", encoding="utf8")
  output_file_correlation_long = open("/home/~~~/deplen_freq/correlations_long.txt", "a", encoding="utf8")

  output_dct = dict()
  output_dct_long = dict()

  parameters_as_string = input_file_parameters.read()
  input_file_parameters.close()
  parameters = parameters_as_string.split(" ")

  token_target = int(parameters[1])
  token_target_long = int(token_target * 2)
  char_target = int(parameters[2])
  char_target_range = int(round((0.1 * char_target), 0))
  char_target_long = int(parameters[3])
  char_target_range_long = int(round((0.1 * char_target_long), 0))
  language = parameters[0]

  output_file_name = "../x_" + language + "_scatter_plot_normal.txt"
  output_file_name_long = "../x_" + language + "_scatter_plot_long.txt"
  output_file = open(output_file_name, "w", encoding="utf8")
  output_file_long = open(output_file_name_long, "w", encoding="utf8")
  output_file.write("SLF\tSDD\n")
  output_file_long.write("SLF\tSDD\n")

  list_of_sum_of_log_freqs = []
  list_of_sum_of_distances = []
  list_of_sum_of_log_freqs_long = []
  list_of_sum_of_distances_long = []

  n_normal = 0
  n_long = 0

  input_file_as_string = input_file.read()
  parses = input_file_as_string.split("\n\n")
  for parse in parses:
    tok_len = 0
    char_len = 0
    tok_condition = 0
    char_condition = 0
    tok_condition_long = 0
    char_condition_long = 0

    lines = parse.split("\n")
    line = lines[0]
    line = line.strip()
    line_elements = line.split("\t")
    if len(line_elements) > 8:
      tok_len = int(line_elements[10])
      char_len = int(line_elements[11])
      if tok_len == token_target:
        tok_condition = 1
      if char_len >= (char_target - char_target_range) and char_len <= (char_target + char_target_range):
        char_condition = 1
      if tok_len == token_target_long:
        tok_condition_long = 1
      if char_len >= (char_target_long - char_target_range_long) and char_len <= (char_target_long + char_target_range_long):
        char_condition_long = 1

    if tok_condition == 1 and char_condition == 1:
      n_normal += 1
      sum_of_log_freqs = 0
      sum_of_distances = 0
      for line in lines:
        line_elements = line.split("\t")
        if len(line_elements) > 8:
          log_freq_lemma = float(line_elements[13])
          dep_len = int(line_elements[14])
          sum_of_log_freqs += log_freq_lemma
          sum_of_distances += dep_len

      list_of_sum_of_log_freqs.append(sum_of_log_freqs)
      list_of_sum_of_distances.append(sum_of_distances)

      output_file_debug.write(str(round(sum_of_log_freqs, 3)) + "\t" + str(sum_of_distances) + "\n")

      #if sum_of_log_freqs > 0 and sum_of_log_freqs < 10:
      #  output_file_debug.write(str(round(sum_of_log_freqs, 3)) + "\t" + str(sum_of_distances) + "\n" + parse + "\n\n")

      sum_of_log_freqs = round(sum_of_log_freqs, 1)
      if sum_of_log_freqs not in output_dct:
        output_dct[sum_of_log_freqs] = str(sum_of_distances)
      else:
        output_dct[sum_of_log_freqs] += "\t" + str(sum_of_distances)


    if tok_condition_long == 1 and char_condition_long == 1:
      n_long += 1
      sum_of_log_freqs = 0
      sum_of_distances = 0
      for line in lines:
        line_elements = line.split("\t")
        if len(line_elements) > 8:
          log_freq_lemma = float(line_elements[13])
          dep_len = int(line_elements[14])
          sum_of_log_freqs += log_freq_lemma
          sum_of_distances += dep_len

      list_of_sum_of_log_freqs_long.append(sum_of_log_freqs)
      list_of_sum_of_distances_long.append(sum_of_distances)

      sum_of_log_freqs = round(sum_of_log_freqs, 1)
      if sum_of_log_freqs not in output_dct_long:
        output_dct_long[sum_of_log_freqs] = str(sum_of_distances)
      else:
        output_dct_long[sum_of_log_freqs] += "\t" + str(sum_of_distances)

  for key in sorted(output_dct):
    values_string = str(output_dct[key])
    values_list = values_string.split("\t")
    for value in values_list:
      output_file.write(str(key) + "\t" + str(value) + "\n")
  for key in sorted(output_dct_long):
    values_string = str(output_dct_long[key])
    values_list = values_string.split("\t")
    for value in values_list:
      output_file_long.write(str(key) + "\t" + str(value) + "\n")

  correlation_output = scipy.stats.pearsonr(list_of_sum_of_log_freqs, list_of_sum_of_distances)
  output_file_correlation.write(language + "\t" + str(round(correlation_output[0], 3)) + "\t" + str(round(correlation_output[1], 4)) + "\t" + str(n_normal) + "\n")

  correlation_output_long = scipy.stats.pearsonr(list_of_sum_of_log_freqs_long, list_of_sum_of_distances_long)
  output_file_correlation_long.write(language + "\t" + str(round(correlation_output_long[0], 3)) + "\t" + str(round(correlation_output_long[1], 4)) + "\t" + str(n_long) + "\n")

  input_file.close()
  output_file.close()
  output_file_long.close()
  output_file_debug.close()
  output_file_correlation.close()
  output_file_correlation_long.close()


# boilerplate
if __name__ == "__main__":
  startTime = datetime.now()
  main()
  print(datetime.now() - startTime)
else:
  print("Script can only run as a stand-alone.")

