#!/usr/bin/python3
# -*- coding: utf-8 -*-
# rapid dev script to clean parses of comments and parses missing lemmas

import sys, re, os
from datetime import datetime


def main():
  input_file = open("corpus_merged.txt", "r", encoding="utf8")
  output_file = open("corpus_cleaned_interstep.txt", "w", encoding="utf8")

  input_file_as_string = input_file.read()
  input_file_as_string = re.sub('\n\n\n+', "\n\n", input_file_as_string)
  input_file_as_string_lines = input_file_as_string.split("\n")
  for line in input_file_as_string_lines:
    if len(line) > 0:
      if line[0] != "#":
        output_file.write(line + "\n")
    else:
      output_file.write(line + "\n")

  input_file.close()
  output_file.close()

  input_file = open("corpus_cleaned_interstep.txt", "r", encoding="utf8")
  output_file = open("corpus_cleaned.txt", "w", encoding="utf8")

  input_file_as_string = input_file.read()
  parses = input_file_as_string.split("\n\n")
  for parse in parses:
    lines_in_parse = parse.split("\n")

    if "\tVERB\t" in parse:
      if "1\t_\t_" not in parse:
        output_file.write(parse + "\n\n")

  input_file.close()
  output_file.close()

  os.remove("corpus_cleaned_interstep.txt")

# boilerplate
if __name__ == "__main__":
  startTime = datetime.now()
  main()
  print(datetime.now() - startTime)
else:
  print("Script can only run as a stand-alone.")

