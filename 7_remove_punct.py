#!/usr/bin/python3
# -*- coding: utf-8 -*-
# rapid dev script to clear parses of punctuation and lemma-less tokens

import sys, re, os
from datetime import datetime
import math


def main():
  input_file = open("corpus_lscfd.txt", "r", encoding="utf8")
  output_file = open("corpus_lscfdnp.txt", "w", encoding="utf8")

  for line in input_file:
    line = line.strip()
    if "\tpunct\t" not in line:
      line_elements = line.split("\t")
      if len(line_elements) > 8:
        lemma = line_elements[2]
        if lemma != "_":
          output_file.write(line + "\n")
      else:
        output_file.write(line + "\n")

  input_file.close()
  output_file.close()


# boilerplate
if __name__ == "__main__":
  startTime = datetime.now()
  main()
  print(datetime.now() - startTime)
else:
  print("Script can only run as a stand-alone.")

