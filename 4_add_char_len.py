#!/usr/bin/python3
# -*- coding: utf-8 -*-
# python course 26 Aug 2018
# rapid dev script to add char length per token to token

import sys, re, os
from datetime import datetime


def main():
  input_file = open("corpus_ls.txt", "r", encoding="utf8")
  output_file = open("corpus_lsc.txt", "w", encoding="utf8")

  input_file_as_string = input_file.read()
  parses = input_file_as_string.split("\n\n")
  for parse in parses:
    lines = parse.split("\n")
    chars_without_punct = ""
    for line in lines:
      line = line.strip()
      if len(line) > 0:
        if "\tpunct\t" not in line:
          parse_elements = line.split("\t")
          if len(parse_elements) > 0:
            orth = parse_elements[1]
            chars_without_punct += orth
    for line in lines:
      line = line.strip()
      if len(line) > 0:
        output_file.write(line + "\t" + str(len(chars_without_punct)) + "\n")
      else:
        output_file.write(line + "\n")
    output_file.write("\n")


  input_file.close()
  output_file.close()


# boilerplate
if __name__ == "__main__":
  startTime = datetime.now()
  main()
  print(datetime.now() - startTime)
else:
  print("Script can only run as a stand-alone.")

